import java.io.*;

public class DataBaseRepository implements Closeable{
    public ObjectInputStream ois;
    public final ObjectOutputStream oos;

    public DataBaseRepository() throws IOException {
        File file = new File("db/db.ser");
            this.ois = new ObjectInputStream(new FileInputStream(file));
            this.oos = new ObjectOutputStream(new FileOutputStream(file));
    }

    @Override
    public void close() throws IOException {
        ois.close();
        oos.close();
    }
}
