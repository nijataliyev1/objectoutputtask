
public class Main {
    public static void main(String[] args) {
        StudentRepository sr1 = new StudentRepository("Nijat", "Aliyev", 20);
        StudentRepository sr2 = new StudentRepository("Ali", "Guluzada", 20);
        try (DataBaseService db = new DataBaseService()) {
            db.save(sr1);
            db.save(sr2);
            System.out.println(db.get(2));
            System.out.println(db.get(1));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}