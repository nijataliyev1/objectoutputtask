import java.io.*;

public class StudentRepository implements Serializable {
    private static final long serialVersionUID = -1123791283042L;
    private int id;

    private static int nextId = 1;
    private String name;
    private String surname;
    private int age;

    public StudentRepository(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.id = nextId++;
    }

    public static int getNextId() {
        return nextId;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "StudentRepository{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
