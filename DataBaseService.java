import java.io.*;

public class DataBaseService implements Closeable {
    private final DataBaseRepository repo;

    public DataBaseService() throws IOException {
        repo = new DataBaseRepository();
        repo.ois.mark(0);
    }

    public void save(StudentRepository o) throws IOException {
        repo.oos.writeObject(o);
    }

    public StudentRepository get(int id) throws IOException, ClassNotFoundException {
        resetOis();
        for (int i = 1; i <= StudentRepository.getNextId(); i++) {
            Object o = repo.ois.readObject();
            if (o instanceof StudentRepository) {
                StudentRepository sr = (StudentRepository) o;
                if (sr.getId() == id) return sr;
            }
        }
        return null;
    }


    private void resetOis() throws IOException {
        this.repo.ois.close();
        this.repo.ois = new ObjectInputStream(new FileInputStream(new File("db/db.ser")));
    }

    @Override
    public void close() throws IOException {
        this.repo.close();
    }
}
